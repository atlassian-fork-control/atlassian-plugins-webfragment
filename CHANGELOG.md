# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [5.0.0 Unreleased]

### Changed
* Source and target Java version is now Java 8 (previously 7)
* Adds support for running Java 11
* Now requires the host product export Platform 5
