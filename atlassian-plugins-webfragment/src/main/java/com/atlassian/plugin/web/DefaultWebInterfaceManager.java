package com.atlassian.plugin.web;

import com.atlassian.annotations.tenancy.TenancyScope;
import com.atlassian.annotations.tenancy.TenantAware;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.WebSection;
import com.atlassian.plugin.web.api.model.WebFragmentBuilder;
import com.atlassian.plugin.web.descriptors.ConditionalDescriptor;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebItemProviderModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionProviderModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WeightedDescriptorComparator;
import com.atlassian.plugin.web.model.WebLink;
import com.atlassian.plugin.web.model.WebPanel;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;

import static com.atlassian.plugin.web.api.model.WeightedComparator.WEIGHTED_FRAGMENT_COMPARATOR;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;

/**
 * Stores and manages flexible web interface sections available in the system.
 */
public class DefaultWebInterfaceManager implements DynamicWebInterfaceManager
{
    private static final Logger log = LoggerFactory.getLogger(DefaultWebInterfaceManager.class);

    public static final WeightedDescriptorComparator WEIGHTED_DESCRIPTOR_COMPARATOR = new WeightedDescriptorComparator();
    private static final long CACHE_EXPIRY = Long.getLong("com.atlassian.plugin.web.interface.caches.timeout.sec", 60 * 60);

    private PluginAccessor pluginAccessor;
    private WebFragmentHelper webFragmentHelper;

    @TenantAware(TenancyScope.TENANTLESS)
    private final LoadingCache<String, List<WebSectionModuleDescriptor>> sections = CacheBuilder.newBuilder().
            expireAfterAccess(CACHE_EXPIRY, TimeUnit.SECONDS).
            build(new WebSectionCacheLoader());
    @TenantAware(TenancyScope.TENANTLESS)
    private final LoadingCache<String, List<WebItemModuleDescriptor>> items = CacheBuilder.newBuilder().
            expireAfterAccess(CACHE_EXPIRY, TimeUnit.SECONDS).
            build(new WebItemCacheLoader());
    @TenantAware(TenancyScope.TENANTLESS)
    private final LoadingCache<String, List<WebPanelModuleDescriptor>> panels = CacheBuilder.newBuilder().
            expireAfterAccess(CACHE_EXPIRY, TimeUnit.SECONDS).
            build(new WebPanelCacheLoader());
    @TenantAware(TenancyScope.TENANTLESS)
    private final LoadingCache<String, List<WebItemProviderModuleDescriptor>> itemProviders = CacheBuilder.newBuilder().
            expireAfterAccess(CACHE_EXPIRY, TimeUnit.SECONDS).
            build(new WebItemProviderCacheLoader());
    @TenantAware(TenancyScope.TENANTLESS)
    private final LoadingCache<String, List<WebSectionProviderModuleDescriptor>> sectionProviders = CacheBuilder.newBuilder().
            expireAfterAccess(CACHE_EXPIRY, TimeUnit.SECONDS).
            build(new WebSectionProviderCacheLoader());

    public DefaultWebInterfaceManager()
    {
        refresh();
    }

    public DefaultWebInterfaceManager(PluginAccessor pluginAccessor, WebFragmentHelper webFragmentHelper)
    {
        this.pluginAccessor = pluginAccessor;
        this.webFragmentHelper = webFragmentHelper;
        refresh();
    }

    public boolean hasSectionsForLocation(String location)
    {
        return !Iterables.isEmpty(getWebSections(location, Collections.<String, Object>emptyMap()));
    }

    public List<WebSectionModuleDescriptor> getSections(String location)
    {
        return location == null ? Collections.<WebSectionModuleDescriptor>emptyList() : sections.getUnchecked(location);
    }

    public List<WebSectionModuleDescriptor> getDisplayableSections(String location, Map<String, Object> context)
    {
        return filterFragmentsByCondition(getSections(location), context);
    }

    public List<WebItemModuleDescriptor> getItems(String section)
    {
        return section == null ? Collections.<WebItemModuleDescriptor>emptyList() : items.getUnchecked(section);
    }

    public List<WebItemModuleDescriptor> getDisplayableItems(String section, Map<String, Object> context)
    {
        return filterFragmentsByCondition(getItems(section), context);
    }

    public List<WebPanel> getDisplayableWebPanels(String location, Map<String, Object> context)
    {
        return toWebPanels(getDisplayableWebPanelDescriptors(location, context));
    }

    public List<WebPanelModuleDescriptor> getDisplayableWebPanelDescriptors(String location, Map<String, Object> context)
    {
        return filterFragmentsByCondition(getWebPanelDescriptors(location), context);
    }

    public List<WebPanel> getWebPanels(String location)
    {
        return toWebPanels(getWebPanelDescriptors(location));
    }

    private List<WebPanel> toWebPanels(List<WebPanelModuleDescriptor> descriptors)
    {
        return Lists.transform(descriptors, new Function<WebPanelModuleDescriptor, WebPanel>()
        {
            public WebPanel apply(WebPanelModuleDescriptor from)
            {
                return from.getModule();
            }
        });
    }

    public List<WebPanelModuleDescriptor> getWebPanelDescriptors(String location)
    {
        return location == null ? Collections.<WebPanelModuleDescriptor>emptyList() : panels.getUnchecked(location);
    }

    private <T extends ConditionalDescriptor> List<T> filterFragmentsByCondition(List<T> relevantItems, Map<String, Object> context)
    {
        if (relevantItems.isEmpty())
        {
            return relevantItems;
        }

        List<T> result = new ArrayList<T>(relevantItems);
        for (Iterator<T> iterator = result.iterator(); iterator.hasNext(); )
        {
            ConditionalDescriptor descriptor = iterator.next();
            try
            {
                if (descriptor.getCondition() != null && !descriptor.getCondition().shouldDisplay(context))
                {
                    iterator.remove();
                }
            }
            catch (Throwable t)
            {
                log.error("Could not evaluate condition '" + descriptor.getCondition() + "' for descriptor: " + descriptor, t);
                iterator.remove();
            }
        }

        return result;
    }

    public void refresh()
    {
        sections.invalidateAll();
        items.invalidateAll();
        panels.invalidateAll();
        itemProviders.invalidateAll();
        sectionProviders.invalidateAll();
    }

    /**
     * @param pluginAccessor The plugin accessor to set
     * @since 2.2.0
     */
    public void setPluginAccessor(PluginAccessor pluginAccessor)
    {
        this.pluginAccessor = pluginAccessor;
    }

    public void setWebFragmentHelper(WebFragmentHelper webFragmentHelper)
    {
        this.webFragmentHelper = webFragmentHelper;
    }

    public WebFragmentHelper getWebFragmentHelper()
    {
        return webFragmentHelper;
    }

    @Override
    public Iterable<WebItem> getWebItems(final String section, final Map<String, Object> context)
    {
        return getDynamicWebItems(getItems(section), section, context);
    }

    @Override
    public Iterable<WebItem> getDisplayableWebItems(final String section, final Map<String, Object> context)
    {
        return getDynamicWebItems(getDisplayableItems(section, context), section, context);
    }

    @Override
    public Iterable<WebSection> getWebSections(final String location, final Map<String, Object> context)
    {
        return getDynamicWebSections(getSections(location), location, context);
    }

    @Override
    public Iterable<WebSection> getDisplayableWebSections(final String location, final Map<String, Object> context)
    {
        return getDynamicWebSections(getDisplayableSections(location, context), location, context);
    }

    private Iterable<WebItem> getDynamicWebItems(final List<WebItemModuleDescriptor> staticItems, final String section, final Map<String, Object> context)
    {
        final List<WebItem> ret = newArrayList(transform(staticItems, new WebItemConverter(context)));
        for (WebItemProviderModuleDescriptor itemProvider : itemProviders.getUnchecked(section))
        {
            try
            {
                final Optional<Iterable<WebItem>> providedItems = ofNullable(itemProvider.getModule().getItems(context));
                if (providedItems.isPresent())
                {
                    Iterables.addAll(ret, providedItems.get());
                }
            }
            catch (RuntimeException e)
            {
                if (log.isDebugEnabled())
                {
                    log.error("WebItemProvider from module '" + itemProvider.getCompleteKey() + "' threw an error '" + e.getMessage() + "'. Web-items provided by this provider will be ignored.", e);
                }
                else
                {
                    log.error("WebItemProvider from module '" + itemProvider.getCompleteKey() + "' threw an error '" + e.getMessage() + "'. Web-items provided by this provider will be ignored.");
                }
            }
        }

        Collections.sort(ret, WEIGHTED_FRAGMENT_COMPARATOR);
        return ret;
    }


    private Iterable<WebSection> getDynamicWebSections(final List<WebSectionModuleDescriptor> staticSections, final String location, final Map<String, Object> context)
    {
        final List<WebSection> ret = newArrayList(transform(staticSections, new WebSectionConverter(context)));
        for (WebSectionProviderModuleDescriptor provider : sectionProviders.getUnchecked(location))
        {
            try
            {
                final Optional<Iterable<WebSection>> sections = ofNullable(provider.getModule().getSections(context));
                if (sections.isPresent())
                {
                    Iterables.addAll(ret, sections.get());
                }
            }
            catch (RuntimeException e)
            {
                if (log.isDebugEnabled())
                {
                    log.error("WebSectionProvider from module '" + provider.getCompleteKey() + "' threw an error '" + e.getMessage() + "'. Web-sections provided by this provider will be ignored.", e);
                }
                else
                {
                    log.error("WebItemProvider from module '" + provider.getCompleteKey() + "' threw an error '" + e.getMessage() + "'. Web-sections provided by this provider will be ignored.");
                }
            }
        }

        Collections.sort(ret, WEIGHTED_FRAGMENT_COMPARATOR);
        return ret;
    }

    private static class WebItemConverter implements Function<WebItemModuleDescriptor, WebItem>
    {
        private final Map<String, Object> context;

        public WebItemConverter(final Map<String, Object> context)
        {
            this.context = context;
        }

        @Override
        public WebItem apply(final WebItemModuleDescriptor input)
        {
            final WebFragmentBuilder builder = new WebFragmentBuilder(input.getCompleteKey(), input.getWeight());
            builder.styleClass(input.getStyleClass());
            if (input.getWebLabel() != null)
            {
                builder.label(input.getWebLabel().getDisplayableLabel(null, context));
            }
            if (input.getTooltip() != null)
            {
                builder.title(input.getTooltip().getDisplayableLabel(null, context));
            }
            if (input.getWebParams() != null)
            {
                builder.params(input.getWebParams().getParams());
            }

            //icon URLs are pretty rare since we use styleclasses with background icons so
            //this is here for backwards compatibility for those rare cases where a dynamic rendered URL may be necessary.
            if (input.getIcon() != null && input.getIcon().getUrl() != null)
            {
                builder.addParam("iconUrl", input.getIcon().getUrl().getRenderedUrl(context));
            }

            final WebFragmentBuilder.WebItemBuilder webItemBuilder = builder.webItem(input.getSection(), input.getEntryPoint());
            final WebLink link = input.getLink();
            if (link != null)
            {
                builder.id(link.getId());
                if (link.hasAccessKey())
                {
                    webItemBuilder.accessKey(link.getAccessKey(context));
                }
                final Optional<HttpServletRequest> httpRequest = ofNullable((HttpServletRequest) context.get("request"));
                final String url = httpRequest.isPresent() ? link.getDisplayableUrl(httpRequest.get(), context) : link.getRenderedUrl(context);
                webItemBuilder.url(url);
            }

            return webItemBuilder.build();
        }
    }

    private static class WebSectionConverter implements Function<WebSectionModuleDescriptor, WebSection>
    {
        private final Map<String, Object> context;

        public WebSectionConverter(final Map<String, Object> context) {this.context = context;}

        @Override
        public WebSection apply(final WebSectionModuleDescriptor input)
        {
            final WebFragmentBuilder builder = new WebFragmentBuilder(input.getCompleteKey(), input.getWeight());
            builder.id(input.getKey());
            if (input.getWebLabel() != null)
            {
                builder.label(input.getWebLabel().getDisplayableLabel(null, context));
            }
            if (input.getTooltip() != null)
            {
                builder.title(input.getTooltip().getDisplayableLabel(null, context));
            }
            if (input.getWebParams() != null)
            {
                builder.params(input.getWebParams().getParams());
            }

            return builder.webSection(input.getLocation()).build();
        }
    }

    private class WebSectionCacheLoader extends CacheLoader<String, List<WebSectionModuleDescriptor>>
    {
        @Override
        public List<WebSectionModuleDescriptor> load(final String location) throws Exception
        {
            final List<WebSectionModuleDescriptor> descriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(WebSectionModuleDescriptor.class);
            final List<WebSectionModuleDescriptor> result = newArrayList(filter(descriptors, new Predicate<WebSectionModuleDescriptor>()
            {
                @Override
                public boolean apply(final WebSectionModuleDescriptor input)
                {
                    return StringUtils.equals(location, input.getLocation());
                }
            }));
            Collections.sort(result, WEIGHTED_DESCRIPTOR_COMPARATOR);
            return result;
        }
    }

    private class WebItemCacheLoader extends CacheLoader<String, List<WebItemModuleDescriptor>>
    {
        @Override
        public List<WebItemModuleDescriptor> load(final String section) throws Exception
        {
            final List<WebItemModuleDescriptor> descriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(WebItemModuleDescriptor.class);
            final List<WebItemModuleDescriptor> result = newArrayList(filter(descriptors, new Predicate<WebItemModuleDescriptor>()
            {
                @Override
                public boolean apply(final WebItemModuleDescriptor input)
                {
                    return StringUtils.equals(section, input.getSection());
                }
            }));
            Collections.sort(result, WEIGHTED_DESCRIPTOR_COMPARATOR);
            return result;
        }
    }

    private class WebPanelCacheLoader extends CacheLoader<String, List<WebPanelModuleDescriptor>>
    {
        @Override
        public List<WebPanelModuleDescriptor> load(final String location) throws Exception
        {
            final List<WebPanelModuleDescriptor> descriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(WebPanelModuleDescriptor.class);
            final List<WebPanelModuleDescriptor> result = newArrayList(filter(descriptors, new Predicate<WebPanelModuleDescriptor>()
            {
                @Override
                public boolean apply(final WebPanelModuleDescriptor input)
                {
                    return StringUtils.equals(location, input.getLocation());
                }
            }));
            Collections.sort(result, WEIGHTED_DESCRIPTOR_COMPARATOR);
            return result;
        }
    }

    private class WebItemProviderCacheLoader extends CacheLoader<String, List<WebItemProviderModuleDescriptor>>
    {
        @Override
        public List<WebItemProviderModuleDescriptor> load(final String section) throws Exception
        {
            final List<WebItemProviderModuleDescriptor> providers = pluginAccessor.getEnabledModuleDescriptorsByClass(WebItemProviderModuleDescriptor.class);
            return newArrayList(filter(providers, new Predicate<WebItemProviderModuleDescriptor>()
            {
                @Override
                public boolean apply(final WebItemProviderModuleDescriptor input)
                {
                    return StringUtils.equals(input.getSection(), section);
                }
            }));
        }
    }

    private class WebSectionProviderCacheLoader extends CacheLoader<String, List<WebSectionProviderModuleDescriptor>>
    {
        @Override
        public List<WebSectionProviderModuleDescriptor> load(final String location) throws Exception
        {
            final List<WebSectionProviderModuleDescriptor> providers = pluginAccessor.getEnabledModuleDescriptorsByClass(WebSectionProviderModuleDescriptor.class);
            return newArrayList(filter(providers, new Predicate<WebSectionProviderModuleDescriptor>()
            {
                @Override
                public boolean apply(final WebSectionProviderModuleDescriptor input)
                {
                    return StringUtils.equals(input.getLocation(), location);
                }
            }));
        }
    }
}
