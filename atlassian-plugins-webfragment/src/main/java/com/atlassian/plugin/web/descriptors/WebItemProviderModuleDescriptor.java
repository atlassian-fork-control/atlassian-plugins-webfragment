package com.atlassian.plugin.web.descriptors;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationPattern;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.api.provider.WebItemProvider;
import com.google.common.base.Supplier;
import org.dom4j.Element;

import javax.annotation.Nonnull;

import static com.atlassian.plugin.util.validation.ValidationPattern.test;
import static com.google.common.base.Suppliers.memoize;

public class WebItemProviderModuleDescriptor extends AbstractModuleDescriptor<WebItemProvider>
{

    private final WebInterfaceManager webInterfaceManager;
    private Supplier<WebItemProvider> itemSupplier;
    private String section;

    public WebItemProviderModuleDescriptor(final ModuleFactory moduleFactory, final WebInterfaceManager webInterfaceManager)
    {
        super(moduleFactory);
        this.webInterfaceManager = webInterfaceManager;
    }

    @Override
    protected void provideValidationRules(ValidationPattern pattern)
    {
        super.provideValidationRules(pattern);
        pattern.rule(test("@class").withError("The web item provider class is required."));
        pattern.rule(test("@section").withError("Must provide a section that items should be added to."));
    }

    @Override
    public void init(@Nonnull final Plugin plugin, @Nonnull final Element element) throws PluginParseException
    {
        super.init(plugin, element);
        final WebItemProviderModuleDescriptor that = this;
        itemSupplier = memoize(new Supplier<WebItemProvider>()
        {
            @Override
            public WebItemProvider get()
            {
                return moduleFactory.createModule(moduleClassName, that);
            }
        });

        section = element.attributeValue("section");
    }

    public String getSection()
    {
        return section;
    }

    @Override
    public WebItemProvider getModule()
    {
        return itemSupplier.get();
    }

    @Override
    public void enabled()
    {
        super.enabled();
        webInterfaceManager.refresh();
    }

    @Override
    public void disabled()
    {
        super.disabled();
        webInterfaceManager.refresh();
    }
}
