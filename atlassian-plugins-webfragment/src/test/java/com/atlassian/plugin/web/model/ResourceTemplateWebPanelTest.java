package com.atlassian.plugin.web.model;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.renderer.RendererException;
import com.atlassian.plugin.web.renderer.WebPanelRenderer;
import junit.framework.TestCase;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collections;
import java.util.Map;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ResourceTemplateWebPanelTest extends TestCase
{
    private static final String TEMPLATE_FILE_TEXT = "This <file> is used as web panel contents in unit tests.\n" +
            "Do not change its contents.";

    private PluginAccessor accessorMock;
    private WebPanelRenderer rendererMock;
    private Plugin pluginMock;
    private Map<String, Object> emptyContext = Collections.emptyMap();

    @Override
    protected void setUp() throws Exception
    {
        accessorMock = mock(PluginAccessor.class);
        rendererMock = mock(WebPanelRenderer.class);
        pluginMock = mock(Plugin.class);
        when(pluginMock.getClassLoader()).thenReturn(this.getClass().getClassLoader());
    }

    @Override
    protected void tearDown() throws Exception
    {
        accessorMock = null;
        rendererMock = null;
        pluginMock = null;
    }

    public void testGetHtml()
    {
        String resourceFilename = "ResourceTemplateWebPanelTest.txt";
        String resourceType = "static";
        final ResourceTemplateWebPanel webPanel = newResourceTemplateWebPanel(resourceFilename, resourceType);

        assertEquals(TEMPLATE_FILE_TEXT, webPanel.getHtml(Collections.<String, Object>emptyMap()));
    }

    public void testWriteHtml() throws IOException
    {
        ResourceTemplateWebPanel webPanel = newResourceTemplateWebPanel("ResourceTemplateWebPanelTest.txt", "static");

        StringWriter out = new StringWriter();
        webPanel.writeHtml(out, emptyContext);
        assertEquals(TEMPLATE_FILE_TEXT, out.toString());
    }

    public void testErrorMessagesEscaped() throws IOException
    {
        WebPanelTestUtils.mockPluginAccessorReturning(accessorMock, rendererMock);
        when(rendererMock.getResourceType()).thenReturn("cheese");

        doThrow(new RendererException("<cheese>")).when(rendererMock).render(eq("<filename>.txt"), same(pluginMock), same(emptyContext), isA(Writer.class));

        ResourceTemplateWebPanel webPanel = newResourceTemplateWebPanel("<filename>.txt", "cheese");
        assertEquals("Error rendering WebPanel (&lt;filename&gt;.txt): &lt;cheese&gt;", webPanel.getHtml(emptyContext));
    }

    public void testErrorMessagesEscapedInWriteHtml() throws IOException
    {
        WebPanelTestUtils.mockPluginAccessorReturning(accessorMock, rendererMock);
        when(rendererMock.getResourceType()).thenReturn("cheese");
        doThrow(new RendererException("<cheese>")).when(rendererMock).render(eq("<filename>.txt"), same(pluginMock), same(emptyContext), isA(Writer.class));

        ResourceTemplateWebPanel webPanel = newResourceTemplateWebPanel("<filename>.txt", "cheese");

        StringWriter out = new StringWriter();
        webPanel.writeHtml(out, emptyContext);
        assertEquals("Error rendering WebPanel (&lt;filename&gt;.txt): &lt;cheese&gt;", out.toString());
    }

    public void testUnsupportedResourceType()
    {
        when(rendererMock.getResourceType()).thenReturn("velocity");
        WebPanelTestUtils.mockPluginAccessorReturning(accessorMock, rendererMock);

        ResourceTemplateWebPanel webPanel = newResourceTemplateWebPanel("ResourceTemplateWebPanelTest.txt", "unsupported-type");

        String result = webPanel.getHtml(emptyContext);
        assertEquals("Error rendering WebPanel (ResourceTemplateWebPanelTest.txt): No renderer found for resource type: unsupported-type", result);
    }

    private ResourceTemplateWebPanel newResourceTemplateWebPanel(String resourceFilename, String resourceType)
    {
        final ResourceTemplateWebPanel webPanel = new ResourceTemplateWebPanel(accessorMock);
        webPanel.setPlugin(pluginMock);
        webPanel.setResourceType(resourceType);
        webPanel.setResourceFilename(resourceFilename);
        return webPanel;
    }
}
