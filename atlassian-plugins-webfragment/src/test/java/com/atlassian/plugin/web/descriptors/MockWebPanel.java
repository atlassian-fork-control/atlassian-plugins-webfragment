package com.atlassian.plugin.web.descriptors;

import com.atlassian.plugin.web.model.WebPanel;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

/**
 * A test WebPanel that outputs the values passed in the context, or a filler
 * message if the context is empty.
 */
public class MockWebPanel implements WebPanel
{
    static final String NOTHING_IN_CONTEXT = "nothing in context";

    public String getHtml(Map<String, Object> context)
    {
        StringWriter out = new StringWriter();
        try
        {
            writeHtml(out, context);
            return out.toString();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void writeHtml(Writer writer, Map<String, Object> context) throws IOException
    {
        if (!context.isEmpty())
        {
            for (Object value : context.values())
            {
                writer.write(value.toString());
            }
        }
        else
        {
            writer.write(NOTHING_IN_CONTEXT);
        }
    }
}
