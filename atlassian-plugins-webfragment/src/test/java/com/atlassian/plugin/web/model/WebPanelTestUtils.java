package com.atlassian.plugin.web.model;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.descriptors.WebPanelRendererModuleDescriptor;
import com.atlassian.plugin.web.renderer.WebPanelRenderer;
import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WebPanelTestUtils
{

    public static void mockPluginAccessorReturning(PluginAccessor accessorMock, WebPanelRenderer... renderers)
    {
        class FakeWebPanelRendererModuleDescriptor extends WebPanelRendererModuleDescriptor
        {
            public FakeWebPanelRendererModuleDescriptor(String moduleClassName, ModuleFactory moduleClassFactory)
            {
                super(moduleClassFactory);
                this.moduleClassName = moduleClassName;
            }
        }
        int i = 0;
        List<WebPanelRendererModuleDescriptor> descriptors = new ArrayList<WebPanelRendererModuleDescriptor>(renderers.length);
        for (WebPanelRenderer webPanelRenderer : renderers)
        {
            ModuleFactory factory = mock(ModuleFactory.class);

            WebPanelRendererModuleDescriptor descriptor = new FakeWebPanelRendererModuleDescriptor("class" + i, factory);
            when(factory.createModule("class" + i, descriptor)).thenReturn(webPanelRenderer);
            descriptors.add(descriptor);
            i++;
        }
        when(accessorMock.getEnabledModuleDescriptorsByClass(WebPanelRendererModuleDescriptor.class)).thenReturn(
            ImmutableList.copyOf(descriptors));
    }
}
