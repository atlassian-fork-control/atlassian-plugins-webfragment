package com.atlassian.plugin.web.model;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Represents a plain text, primarily used as a links name
 *
 * @deprecated as of 3.0.2 use {@link com.atlassian.plugin.web.api.WebFragment#getLabel()}
 */
public interface WebLabel extends WebParam
{
    String getKey();

    String getNoKeyValue();

    String getDisplayableLabel(HttpServletRequest req, Map<String,Object> context);
}
