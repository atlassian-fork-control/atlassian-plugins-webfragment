package com.atlassian.plugin.web.api.model;

import com.atlassian.plugin.web.api.WebFragment;

import java.util.Map;
import javax.annotation.Nonnull;

public abstract class AbstractWebFragment implements WebFragment
{
    private final String completeKey;
    private final String label;
    private final String title;
    private final String styleClass;
    private final String id;
    private final Map<String, String> params;
    private final int weight;

    protected AbstractWebFragment(final String completeKey, final String label, final String title, final String styleClass, final String id, final Map<String, String> params, final int weight)
    {
        this.completeKey = completeKey;
        this.label = label;
        this.title = title;
        this.styleClass = styleClass;
        this.id = id;
        this.params = params;
        this.weight = weight;
    }

    @Override
    public String getCompleteKey()
    {
        return completeKey;
    }

    @Override
    public String getLabel()
    {
        return label;
    }

    @Override
    public String getTitle()
    {
        return title;
    }

    @Override
    public String getStyleClass()
    {
        return styleClass;
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Nonnull
    @Override
    public Map<String, String> getParams()
    {
        return params;
    }

    @Override
    public int getWeight()
    {
        return weight;
    }

    @Override
    public String toString()
    {
        return getClass().getName() + "{" + toStringOfFields() + "}";
    }

    protected String toStringOfFields()
    {
        return "completeKey=" + completeKey + ", label=" + label + ", title=" + title + ", styleClass=" + styleClass
                + ", id=" + id + ", params=" + params + ", weight=" + weight;
    }
}
